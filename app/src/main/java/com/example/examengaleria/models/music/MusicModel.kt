package com.example.examengaleria.models.music

import java.io.Serializable

class MusicModel(
    var title: String = "",
    var description: String = "",
    var image: Int = 0,
    var singer: String = "",
    var song: Int = 0
) : Serializable