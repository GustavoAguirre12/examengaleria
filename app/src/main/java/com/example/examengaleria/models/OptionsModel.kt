package com.example.examengaleria.models

import android.content.Intent

class OptionsModel (
    var title: String = "",
    var intent: Intent = Intent()
)