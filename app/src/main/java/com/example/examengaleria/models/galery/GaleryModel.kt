package com.example.examengaleria.models.galery

import java.io.Serializable
import java.util.*

class GaleryModel(
    var title: String = "",
    var description: String = "",
    var date: Date = Date(),
    var image: Int = 0
): Serializable