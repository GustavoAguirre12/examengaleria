package com.example.examengaleria.adapters.galery

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.examengaleria.R
import com.example.examengaleria.adapters.OptionsAdapter
import com.example.examengaleria.models.OptionsModel
import com.example.examengaleria.models.galery.GaleryModel
import com.google.android.material.card.MaterialCardView
import java.util.*
import kotlin.collections.ArrayList

abstract class GaleryAdapter(
    val mContext: Context,
    val list: ArrayList<GaleryModel>
) : RecyclerView.Adapter<GaleryAdapter.RecyclerViewHolder>() {

    inner class RecyclerViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(
            inflater.inflate(
                R.layout.view_holder_galery_adapter,
                parent,
                false
            )
        ) {

        private var cardView: MaterialCardView? = null
        private var txtTitle: TextView? = null
        private var imgDetail: ImageView? = null

        init {
            txtTitle = itemView.findViewById(R.id.txtTitle)
            cardView = itemView.findViewById(R.id.cardView)
            imgDetail = itemView.findViewById(R.id.imgDetail)
        }

        fun bind(item: GaleryModel) {
            txtTitle!!.text = item.title
            imgDetail!!.setImageResource(item.image)

            cardView!!.setOnClickListener {
                getImage(item)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return RecyclerViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val student = list[position]
        holder.bind(student)
    }

    abstract fun getImage(item: GaleryModel)
}