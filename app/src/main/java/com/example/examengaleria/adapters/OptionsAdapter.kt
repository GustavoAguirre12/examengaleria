package com.example.examengaleria.adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.examengaleria.R
import com.example.examengaleria.models.OptionsModel
import com.google.android.material.card.MaterialCardView
import java.util.*
import kotlin.collections.ArrayList

abstract class OptionsAdapter(
    val mContext: Context,
    val list: ArrayList<OptionsModel>
) : RecyclerView.Adapter<OptionsAdapter.RecyclerViewHolder>() {

    inner class RecyclerViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(
            inflater.inflate(
                R.layout.view_holder_options_adapter,
                parent,
                false
            )
        ) {

        private var cardView: MaterialCardView? = null
        private var txtTitle: TextView? = null

        init {
            txtTitle = itemView.findViewById(R.id.txtTitle)
            cardView = itemView.findViewById(R.id.cardView)
        }

        fun bind(position: Int) {
            val randomColor = colorsList[position]
            val item = list[position]

            txtTitle!!.text = item.title
            cardView!!.setCardBackgroundColor(Color.parseColor(randomColor))
            cardView!!.setOnClickListener {
                getOption(item)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return RecyclerViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        holder.bind(position)
    }

    abstract fun getOption(item: OptionsModel)

    companion object {
        val colorsList = arrayListOf<String>(
            "#3949AB",
            "#B71C1C",
            "#388E3C"
        )

        val rand = Random()
    }
}