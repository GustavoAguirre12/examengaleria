package com.example.examengaleria.activities.galery

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.example.examengaleria.R
import com.example.examengaleria.adapters.galery.GaleryAdapter
import com.example.examengaleria.models.galery.GaleryModel
import kotlinx.android.synthetic.main.activity_galery.*
import java.util.*
import kotlin.collections.ArrayList

class GaleryActivity : AppCompatActivity() {

    var galeryList: ArrayList<GaleryModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_galery)

        setSupportActionBar(toolbar)

        toolbar.navigationIcon =
            ContextCompat.getDrawable(this@GaleryActivity, R.drawable.ic_baseline_arrow_back_24)
        toolbar.setNavigationOnClickListener { finish() }

        initialize()

        val galeryAdapter = object : GaleryAdapter(this@GaleryActivity, galeryList) {
            override fun getImage(item: GaleryModel) {
                val intent: Intent = Intent(this@GaleryActivity, GaleryDetailActivity::class.java)
                intent.putExtra("galery", item)
                startActivity(intent)
            }
        }

        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(this@GaleryActivity, 2)
            adapter = galeryAdapter
        }
    }

    private fun initialize() {
        galeryList = arrayListOf<GaleryModel>(
            GaleryModel("img1", "Esta es la imagen 1", Date(), R.drawable.img1),
            GaleryModel("img2", "Esta es la imagen 2", Date(), R.drawable.img2),
            GaleryModel("img3", "Esta es la imagen 3", Date(), R.drawable.img3),
            GaleryModel("img4", "Esta es la imagen 4", Date(), R.drawable.img4),
            GaleryModel("img5", "Esta es la imagen 5", Date(), R.drawable.img5),
            GaleryModel("img6", "Esta es la imagen 6", Date(), R.drawable.img6)
        )
    }
}