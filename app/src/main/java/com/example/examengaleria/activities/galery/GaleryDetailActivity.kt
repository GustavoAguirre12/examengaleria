package com.example.examengaleria.activities.galery

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.examengaleria.R
import com.example.examengaleria.models.galery.GaleryModel
import kotlinx.android.synthetic.main.activity_galery.*
import kotlinx.android.synthetic.main.activity_galery.toolbar
import kotlinx.android.synthetic.main.activity_galery_detail.*

class GaleryDetailActivity : AppCompatActivity() {

    var galery: GaleryModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_galery_detail)

        toolbar.navigationIcon =
            ContextCompat.getDrawable(
                this@GaleryDetailActivity,
                R.drawable.ic_baseline_arrow_back_24
            )
        toolbar.setNavigationOnClickListener { finish() }

        if (savedInstanceState != null) {
            galery = savedInstanceState.getSerializable("galery") as GaleryModel?
        }

        galery = intent.extras?.getSerializable("galery") as GaleryModel?

        txtTitle.text = galery?.title
        txtDescription.text = galery?.description
        imgDetail.setImageResource(galery?.image!!)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putSerializable("galery", galery)
    }
}