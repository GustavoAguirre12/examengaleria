package com.example.examengaleria.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.examengaleria.R
import com.example.examengaleria.activities.galery.GaleryActivity
import com.example.examengaleria.activities.music.MusicActivity
import com.example.examengaleria.activities.profile.ProfileActivity
import com.example.examengaleria.adapters.OptionsAdapter
import com.example.examengaleria.models.OptionsModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val list = arrayListOf<OptionsModel>(
            OptionsModel("Galeria", Intent(this@MainActivity, GaleryActivity::class.java)),
            OptionsModel("Musica", Intent(this@MainActivity, MusicActivity::class.java))
            //OptionsModel("Mi Perfil", Intent(this@MainActivity, ProfileActivity::class.java))
        )

        val optionsAdapter: OptionsAdapter = object : OptionsAdapter(this@MainActivity, list) {
            override fun getOption(item: OptionsModel) {
                startActivity(item.intent)
            }
        }

        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = optionsAdapter
        }
    }
}