package com.example.examengaleria.activities.music

import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import android.view.View
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.examengaleria.R
import com.example.examengaleria.models.music.MusicModel
import kotlinx.android.synthetic.main.activity_music.*


class MusicActivity : AppCompatActivity(), MediaPlayer.OnPreparedListener {

    var mp: MediaPlayer? = null
    var totalTime = 0
    var fileCounter = 0

    var playlist: ArrayList<MusicModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_music)

        setSupportActionBar(toolbar)

        toolbar.navigationIcon =
            ContextCompat.getDrawable(this@MusicActivity, R.drawable.ic_baseline_arrow_back_24)
        toolbar.setNavigationOnClickListener { finish() }

        playlist.clear()
        playlist.add(
            MusicModel(
                "Tears are falling",
                "Tears are falling",
                R.drawable.play1,
                "Lee Sang Gon",
                R.raw.music1
            )
        )
        playlist.add(
            MusicModel(
                "Attention",
                "Attention",
                R.drawable.play2,
                "Charlie Puth",
                R.raw.music2
            )
        )

        mp = MediaPlayer.create(this@MusicActivity, playlist[fileCounter].song)
        mp?.isLooping = true
        mp?.seekTo(0)
        mp?.setVolume(0.5f, 0.5f)
        mp?.setOnPreparedListener(this)

        totalTime = mp?.duration!!

        txtName.text = "${playlist[fileCounter].singer} - ${playlist[fileCounter].title}"
        imgCover.setImageResource(playlist[fileCounter].image)

        elapsedTimeLabel.text = "0:00"
        val remainingTime: String = createTimeLabel(totalTime)
        remainingTimeLabel.text = "- $remainingTime"

        mp?.setOnCompletionListener {
            mp?.reset()
            mp?.release()

            playNextSong()
        }

        positionBar.max = totalTime
        positionBar.setOnSeekBarChangeListener(
            object : OnSeekBarChangeListener {
                override fun onProgressChanged(
                    seekBar: SeekBar,
                    progress: Int,
                    fromUser: Boolean
                ) {
                    if (fromUser) {
                        mp?.seekTo(progress)
                        positionBar.progress = progress
                    }
                }

                override fun onStartTrackingTouch(seekBar: SeekBar) {}
                override fun onStopTrackingTouch(seekBar: SeekBar) {}
            }
        )

        volumeBar.setOnSeekBarChangeListener(
            object : OnSeekBarChangeListener {
                override fun onProgressChanged(
                    seekBar: SeekBar,
                    progress: Int,
                    fromUser: Boolean
                ) {
                    val volumeNum = progress / 100f
                    mp?.setVolume(volumeNum, volumeNum)
                }

                override fun onStartTrackingTouch(seekBar: SeekBar) {}
                override fun onStopTrackingTouch(seekBar: SeekBar) {}
            }
        )
    }

    private val updateSong = object : Runnable {
        override fun run() {
            val msg = Message()
            msg.what = mp?.currentPosition!!
            handler.sendMessage(msg)

            handler.postDelayed(this, 100)
        }
    }

    private val handler: Handler = object : Handler() {
        override fun handleMessage(msg: Message) {
            val currentPosition = msg.what
            positionBar.progress = currentPosition
            val elapsedTime: String = createTimeLabel(currentPosition)
            elapsedTimeLabel.text = elapsedTime
            val remainingTime: String = createTimeLabel(totalTime - currentPosition)
            remainingTimeLabel.text = "- $remainingTime"
        }
    }

    fun createTimeLabel(time: Int): String {
        var timeLabel: String = ""
        val min = time / 1000 / 60
        val sec = time / 1000 % 60
        timeLabel = "$min:"
        if (sec < 10) timeLabel += "0"
        timeLabel += sec
        return timeLabel
    }

    fun playBtnClick(view: View?) {
        if (!mp?.isPlaying!!) {
            mp?.start()
            playBtn.setBackgroundResource(R.drawable.ic_outline_stop_24)
            handler?.postDelayed(updateSong, 100)
        } else {
            mp?.pause()
            playBtn.setBackgroundResource(R.drawable.ic_outline_play_arrow_24)
        }
    }

    fun nextSong(view: View?) {
        playNextSong()
    }

    private fun playNextSong() {
        stopPlaying()

        fileCounter++
        if (fileCounter == playlist.size) {
            fileCounter = 0
        }

        try {
            mp = MediaPlayer.create(this@MusicActivity, playlist[fileCounter].song)

            mp?.start()
            playBtn.setBackgroundResource(R.drawable.ic_outline_stop_24)

            totalTime = mp?.duration!!
            positionBar.max = totalTime

            elapsedTimeLabel.text = "0:00"
            val remainingTime: String = createTimeLabel(totalTime)
            remainingTimeLabel.text = "- $remainingTime"

            txtName.text = "${playlist[fileCounter].singer} - ${playlist[fileCounter].title}"
            imgCover.setImageResource(playlist[fileCounter].image)

            handler.postDelayed(updateSong, 100)
        } catch (e: Exception) {
            Log.e("ERROR", "ERROR ${e.message}")
        }
    }

    private fun stopPlaying() {
        if (mp != null) {
            mp?.stop()
            mp?.reset()
            mp?.release()

            handler?.removeCallbacks(updateSong)
        }
    }

    override fun onStop() {
        super.onStop()

        mp?.stop()
    }

    override fun onDestroy() {
        super.onDestroy()

        stopPlaying()
        mp = null
    }

    override fun onPrepared(mp: MediaPlayer?) {
        Log.e("FILE", "FILE $fileCounter")
        if (fileCounter > 0) {
            mp?.start()
            playBtn.setBackgroundResource(R.drawable.ic_outline_stop_24)
        }

        totalTime = mp?.duration!!
        positionBar.max = totalTime

        elapsedTimeLabel.text = "0:00"
        val remainingTime: String = createTimeLabel(totalTime)
        remainingTimeLabel.text = "- $remainingTime"

        handler.postDelayed(updateSong, 100)
    }
}